# Base on dokken debian image
FROM debian:10

ENV DEBIAN_FRONTEND noninteractive
RUN /usr/bin/apt-get update && \
    /usr/bin/apt-get -y install \
      apt-transport-https \
      apt-utils \
      ca-certificates \
      chef \
      dbus \
      locales \
      lsb-release \
      ifupdown \
      procps \
      ruby-shadow \
      systemd \
      udev
RUN  rm /etc/systemd/system/getty.target.wants/getty\@tty1.service # Remove annoying tty service which consumes 100% CPU

# Allow resolvconf installation
RUN echo "resolvconf resolvconf/linkify-resolvconf boolean false" | debconf-set-selections

COPY collabora-sysadmin-tools.list /etc/apt/sources.list.d/
COPY collabora-archive-keyring.gpg /etc/apt/trusted.gpg.d
RUN apt update && apt install -y collabora-archive-keyring

# Start daemons even when in docker for testing
RUN rm -f /usr/sbin/policy-rc.d

CMD [ "/bin/systemd" ]
